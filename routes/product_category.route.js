var express = require('express');
var router = express.Router();
var controller = require('../controllers/product_category.controller');

router.get('/get',controller.listProductCategory);
router.post('/add',controller.addProductCategory);
router.delete('/delete/:id_product/:id_category',controller.deleteProductCategory);
module.exports = router;