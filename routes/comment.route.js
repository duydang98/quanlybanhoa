var express = require('express');
var router = express.Router();
var controller = require('../controllers/comment.controller');
var UserMiddleware = require('../middlewares/User.middleware');
var CommentMiddleware = require('../middlewares/comment.middleware');
router.get('/:id',controller.getComment);
router.get('/reply/:id',controller.getCommentReply);
router.post('/',UserMiddleware.checkTokenComment,controller.addComment);
router.post('/:id',UserMiddleware.checkTokenComment,CommentMiddleware.checkComment,controller.addReplyComment);
router.put('/:id',UserMiddleware.checkTokenComment,CommentMiddleware.checkComment,controller.updateComment);
router.delete('/:id',UserMiddleware.checkTokenComment,controller.deleteComment);
module.exports = router;