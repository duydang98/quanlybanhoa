var express = require('express');
var router = express.Router();
var controller = require('../controllers/area.controller');

router.get('/get',controller.listArea);
router.post('/add',controller.addArea);
router.get('/update/:id',controller.updateGetArea);
router.put('/update/:id',controller.updateArea);
router.delete('/delete/:id',controller.deleteArea);
module.exports = router;