var express = require('express');
var router = express.Router();
var controller = require('../controllers/orders_import_item.controller');
router.get('/get',controller.listOrderImportItem);
router.get('/get/:id',controller.OrderImportItem);
router.post('/add',controller.addOrderImportItem);
router.put('/update/:id_order/:id_product',controller.updateOrderImportItem);

//router.get('/update/:id',controller.updateGetOrderImport);
//router.put('/update/:id',controller.updateOrderImport);
module.exports = router;