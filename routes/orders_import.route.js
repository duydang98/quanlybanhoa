var express = require('express');
var router = express.Router();
var controller = require('../controllers/orders_import.controller');
router.get('/get',controller.listOrderImport);
router.post('/add',controller.addOrderImport);
router.get('/update/:id',controller.updateGetOrderImport);
router.put('/update/:id',controller.updateOrderImport);
module.exports = router;