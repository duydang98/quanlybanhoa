var express = require('express');
var router = express.Router();
var controller = require('../controllers/promotion.controller');
var UserMiddleware = require('../middlewares/User.middleware');
var PromotionMiddleware = require('../middlewares/promotion.middleware');

router.get('/get_promotion',controller.getPromotion);

router.get('/list_promotion',UserMiddleware.checkTokenEmployee,controller.listPromotion);
router.post('/',UserMiddleware.checkTokenEmployee,PromotionMiddleware.checkPromotion,controller.addPromotion);
router.put('/:id',UserMiddleware.checkTokenEmployee,controller.updatePromotion);
router.delete('/:id',UserMiddleware.checkTokenEmployee,controller.deletePromotion);
module.exports = router;