var express = require('express');
var router = express.Router();
var controller = require('../controllers/user.controller');
var UserMiddleware = require('../middlewares/User.middleware');
//var updateUserMiddleware = require('../middlewares/updateUser.middleware');

var upload = require('../services/multer');
router.post('/register',upload.array('avatar_user'),UserMiddleware.authRegister,controller.registerUser);
router.get('/get/:id',UserMiddleware.checkToken,controller.getUser);
router.put('/update/:id',upload.array('avatar_user'),UserMiddleware.checkToken,UserMiddleware.authUpdate,controller.updateUser);
router.put('/changepassword/:id',UserMiddleware.checkToken,UserMiddleware.authChangePassword,controller.changePassword);
router.post('/login',UserMiddleware.checkLogin,controller.loginUser);
router.get('/me',controller.me);

module.exports = router;


