var express = require('express');
var router = express.Router();
var controller = require('../controllers/category.controller');

router.get('/get',controller.listCategory);
router.post('/add',controller.addCategory);
router.get('/update/:id',controller.updateGetCategory);
router.put('/update/:id',controller.updateCategory);
router.delete('/delete/:id',controller.deleteCategory);
module.exports = router;