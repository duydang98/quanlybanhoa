var express = require('express');
var router = express.Router();
var controller = require('../controllers/report.controller');

router.get('/year/:year',controller.statisticsRevenueYear);
router.get('/month/:month/:year',controller.statisticsRevenueMonth);
module.exports = router;