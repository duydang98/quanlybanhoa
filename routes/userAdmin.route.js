var express = require('express');
var router = express.Router();
var controller = require('../controllers/userAdmin.controller');
var UserMiddleware = require('../middlewares/User.middleware');
var upload = require('../services/multer');

router.get('/employee/list',controller.listEmployee);
router.post('/employee/add',upload.array('avatar_user'),UserMiddleware.authRegister,controller.addEmployee);
router.get('/employee/detail/:id',controller.detailEmployee);
router.delete('/employee/delete/:id',controller.deleteEmployee);
router.put('/employee/update/:id',upload.array('avatar_user'),UserMiddleware.authUpdate,controller.updateEmployee);

router.get('/customer/list',controller.listCustomer);
router.delete('/customer/delete/:id',controller.deleteCustomer);

module.exports = router;