var express = require('express');
var router = express.Router();
var controller = require('../controllers/order.controller');
var UserMiddleware = require('../middlewares/User.middleware');
var OrderMiddleware = require('../middlewares/order.middleware');

router.get('/my_order/:id',UserMiddleware.checkToken,controller.getOrder);
router.get('/order_detail/:id',UserMiddleware.checkTokenOrder,controller.getOneOrder);
router.post('/add',OrderMiddleware.checkTokenAdd,OrderMiddleware.checkAddOrder,controller.addOrder);

router.put('/update/:id',OrderMiddleware.checkTokenUpdate,OrderMiddleware.checkUpdateOrder,controller.updateInformationRecipient);

router.get('/admin',UserMiddleware.checkTokenEmployee,controller.getFullOrder);

router.put('/admin/update/:id',UserMiddleware.checkTokenEmployee,OrderMiddleware.checkUpdateOrderAdmin,controller.updateInformationAdmin);
router.put('/admin/refund/:id',UserMiddleware.checkTokenEmployee,controller.refundMoney);

module.exports = router;