var express = require('express');
var router = express.Router();
var controller = require('../controllers/product.controller');
router.get('/',controller.listProduct);
router.get('/detail/:id',controller.detailProduct);
router.get('/new',controller.getNewProduct);
module.exports = router;