var express = require('express');
var router = new express.Router();
var controller = require('../controllers/distributor.controller');
var distributorMiddleware = require('../middlewares/Distributor.middleware');
//router.get

router.get('/get',controller.listDistributor);
router.post('/add',distributorMiddleware.authDistributor,controller.addDistributor);
router.get('/update/:id',controller.updateGetDistributor);
router.put('/update/:id',distributorMiddleware.authDistributor,controller.updateDistributor);
router.delete('/delete/:id',controller.deleteDistributor);
module.exports = router;