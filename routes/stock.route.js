var express = require('express');
var router = express.Router();
var controller = require('../controllers/stock.controller');

router.get('/get',controller.listStock);
router.post('/add',controller.addStock);
router.get('/update/:id_product/:id_area',controller.updateGetStock);
router.put('/update/:id_product/:id_area',controller.updateStock);
router.delete('/delete/:id_product/:id_area',controller.deleteStock);
module.exports = router;