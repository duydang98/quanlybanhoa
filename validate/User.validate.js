var Joi = require('@hapi/joi');

module.exports.userSchema = Joi.object().keys({
    email: Joi.string().email().lowercase().required(),
    phone_user: Joi.string().required(),
    birthday_user: Joi.date().iso(),   
    sex_user: Joi.string().valid('M', 'F', 'MALE', 'FEMALE').uppercase().required(),
    name_user: Joi.string().required(),
    avatar_user: Joi.string(),
    password: Joi.string().min(7).alphanum().required().strict(),
    address_user: Joi.string().required()//,
    //type: Joi.string().valid('customer','employee','admin').required()
});

module.exports.userUpdateSchema = Joi.object().keys({
    //email: Joi.string().email().lowercase(),
    phone_user: Joi.string().regex(/^((09|03|07|08|05)|(9|3|7|8|5))[0-9]{8}$/),
    birthday_user: Joi.date().iso(),   
    sex_user: Joi.string().valid('M', 'F', 'MALE', 'FEMALE').uppercase(),
    name_user: Joi.string(),
    avatar_user: Joi.string(),
    //password: Joi.string().min(7).alphanum().strict(),
    address_user: Joi.string()
});

module.exports.passwordChangeSchema = Joi.object().keys({
    password: Joi.string().min(7).alphanum().required(),
    newPassword: Joi.string().min(7).alphanum().required(),
    confirmNewPassword: Joi.string().valid(Joi.ref('newPassword')).required()
});

module.exports.loginSchema = Joi.object().keys({
    email: Joi.string().email().lowercase().required(),
    password: Joi.string().min(7).alphanum().required()
});


module.exports.employeeSchema = Joi.object().keys({
    email: Joi.string().email().lowercase().required(),
    phone_user: Joi.string().regex(/^((09|03|07|08|05)|(9|3|7|8|5))[0-9]{8}$/).required(),
    birthday_user: Joi.date().iso(),   
    sex_user: Joi.string().valid('M', 'F', 'MALE', 'FEMALE').uppercase().required(),
    name_user: Joi.string().required(),
    avatar_user: Joi.string(),
    password: Joi.string().min(7).alphanum().required().strict(),
    address_user: Joi.string().required()//,
    //type: Joi.string().valid('employee').required()
});
