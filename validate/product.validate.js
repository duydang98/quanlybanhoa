var Joi = require('@hapi/joi');

module.exports.productSchema = Joi.object().keys({
    name_product: Joi.string().required(),
    price_product: Joi.number().required(),
    unit_product: Joi.string().required(),
    description_product: Joi.string().required(),
    image: Joi.string()
});