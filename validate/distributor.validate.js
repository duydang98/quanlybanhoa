var Joi = require('@hapi/joi');

module.exports.distributorSchema = Joi.object().keys({
    
    phone_distributor: Joi.string().required(),
   
    name_distributor: Joi.string().required(),
   
    address_distributor: Joi.string().required(),
    
    email: Joi.string().email().lowercase().required()
    //type: Joi.string().valid('customer','employee','admin').required()
});