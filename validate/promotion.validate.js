var Joi = require('@hapi/joi');

module.exports.promotionSchema = Joi.object().keys({
    name_promotion: Joi.string().required(),
    content_promotion: Joi.string().required(),
    date_start: Joi.date().required(),
    date_finish: Joi.date().min(Joi.ref('date_start')),
    quantity_promotion: Joi.number(),
    type_promotion: Joi.string().valid('Value discount','Percentage discount','BOGO','Free gift','Free shipping').required(),
    price_promotion: Joi.number(),
    id_product: Joi.number().required()
});