var Joi = require('@hapi/joi');

module.exports.orderUser = Joi.object().keys({
    name_recipient: Joi.string().required(),
    phone_recipient: Joi.string().required(),	
    address_recipient: Joi.string().required(),	
    payment_method:  Joi.string().valid('COD','stripe').required(),
    order_items: Joi.array().required(),
    // order_items: 	Joi.array().items(Joi.object({
    //     id_product: Joi.number().required(),
    //     quantity_product: Joi.number().required(),
    //     id_area: Joi.number().required(),
    //     id_promotion: Joi.number()
    // })),
    source_id: Joi.string(),
    source_email: Joi.string()
   
});

module.exports.updateOrderUser = Joi.object().keys({
    name_recipient: Joi.string(),
    phone_recipient: Joi.string().regex(/^((09|03|07|08|05)|(9|3|7|8|5))[0-9]{8}$/) ,	
    address_recipient: Joi.string()
});

module.exports.updateOrderAdmin = Joi.object().keys({
    status_order: Joi.string().valid('Pending','Awaiting Shipment','Shipped','Completed','Cancelled'),
    date_delivery: Joi.date().iso(),
    name_shipper: Joi.string(),
    phone_shipper: Joi.string().regex(/^((09|03|07|08|05)|(9|3|7|8|5))[0-9]{8}$/) ,	
    address_shipper: Joi.string()
    
});