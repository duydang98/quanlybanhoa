var jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
dotenv.config();

module.exports =  (token)=>{
   var decoded =   jwt.verify(token,process.env.JWT_SECRET);
   return decoded.id_user;
}