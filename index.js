var express = require('express');
var cors = require('cors');
var app = new express();
app.use(cors());

var port = 8080;

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

var sequelize = require('./configs/conection');

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
});

app.use(express.static('./public')); 
app.get('/',(req,res)=>{
    res.send("index");

});

var middleware = require('./middlewares/User.middleware');
var routeUser = require('./routes/user.route');
var routeProduct = require('./routes/product.route');
var routeComment = require('./routes/comment.route');
//admin
var routeDistributor = require('./routes/distributor.route');
var routeUserAdmin = require('./routes/userAdmin.route');
var routecategory = require('./routes/category.route');
var routeArea = require('./routes/area.route');
var routeAdminProduct = require('./routes/productAdmin.route');
var routeStock = require('./routes/stock.route');
var routeProductCategory = require('./routes/product_category.route');
var routeOrderImport = require('./routes/orders_import.route');
var routeOrderImportItem = require('./routes/orders_import_item.route');
var routePromotion = require('./routes/promotion.route');
var routeOrder = require('./routes/order.route');
var routeRevenue = require('./routes/report.route');
app.use('/user',routeUser);
app.use('/product',routeProduct);
app.use('/comment',routeComment);
app.use('/promotion',routePromotion);
app.use('/order',routeOrder);
//admin

app.use('/user/admin',middleware.checkTokenAdmin,routeUserAdmin);
app.use('/admin/distributor',middleware.checkTokenAdmin,routeDistributor);
app.use('/admin/category',middleware.checkTokenAdmin,routecategory);
app.use('/admin/area',middleware.checkTokenAdmin,routeArea);
app.use('/admin/product',middleware.checkTokenAdmin,routeAdminProduct);
app.use('/admin/stock',middleware.checkTokenAdmin,routeStock);
app.use('/admin/productcategory',middleware.checkTokenAdmin,routeProductCategory);
app.use('/admin/order_import',middleware.checkTokenEmployee,routeOrderImport);
app.use('/admin/order_import_item',middleware.checkTokenEmployee,routeOrderImportItem);
app.use('/admin/statistics_revenue',middleware.checkTokenAdmin,routeRevenue);
app.listen(port,()=>{
    console.log("server start on port "+ port);
});