var OrderItem = require('../models/order_import_item.model');
var Product = require('../models/product.model');
var OrderImport = require('../models/order_import.model');
module.exports.listOrderImportItem = async (req,res)=>{
    var orderitem = await OrderItem.findAll();
    res.json(orderitem);
}

module.exports.OrderImportItem = async (req,res)=>{
    var id = req.params.id;
    var orderitem = await OrderItem.findAll({where: {id_order_import: id}});
    res.json(orderitem);
}

module.exports.addOrderImportItem = async (req,res)=>{
    var orderItem = req.body;
    var orderimport = await OrderImport.findOne({where: {id_order_import: orderItem.id_order_import}});
    if(!orderimport){
        res.json({result: "Order not found"});
        return;
    }
    var product = await Product.findOne({where: {id_product: orderItem.id_product}});
    if(!product){
        res.json({result: "Product not found"});
        return;
    }
    orderItem.name_product = product.name_product;
    await OrderItem.create(orderItem);
    res.json(orderItem);
}

module.exports.updateOrderImportItem = async (req,res)=>{
    var orderItem = req.body;
    var id_order = req.params.id_order;
    var id_product = req.params.id_product;
    var orderimport = await OrderImport.findOne({where: {id_order_import: id_order}});
    if(!orderimport){
        res.json({result: "Order not found"});
        return;
    }
    var product = await Product.findOne({where: {id_product: id_product}});
    if(!product){
        res.json({result: "Product not found"});
        return;
    }
    orderItem.name_product = product.name_product;
    await OrderItem.update(orderItem,{where: {id_order_import: id_order,id_product: id_product}});
    res.json(orderItem);
    
}