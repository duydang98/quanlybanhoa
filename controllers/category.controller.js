var Category = require('../models/category.model');

module.exports.listCategory = async(req,res)=>{
    var categorys = await Category.findAll();
    res.json(categorys);
}

module.exports.addCategory = async (req,res)=>{
    var category = await Category.create(req.body);
    res.json(category);
}

module.exports.updateGetCategory = async(req,res)=>{
    var id = req.params.id;
    var category = await Category.findOne({where: {id_category : id}});
    res.json(category);
}

module.exports.updateCategory = async(req,res)=>{
    var id = req.params.id;
    await Category.update(req.body,{where: {id_category: id}});
    res.json({
        result: "suscess"
    });
}

module.exports.deleteCategory = async(req,res)=>{
    var id = req.params.id;
    await Category.destroy({where: {id_category : id}});
    res.json({
        result: "suscess"
    });
}