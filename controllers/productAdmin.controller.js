var Product = require('../models/product.model');
var cloudinary = require('../services/cloudinary');
var fs = require('fs');
var Stock = require('../models/stock.model');

Product.hasMany(Stock,{as: "Stock",foreignKey: 'id_product'});

module.exports.listProduct = async (req,res)=>{
    var products = await Product.findAll({
        include: [{
            model: Stock, as: 'Stock'
          }]
    });
    res.json(products);
};

module.exports.addProduct = async (req,res)=>{
    if(req.files){
        const uploader = async (path) => await cloudinary.uploads(path, 'Images/products');
        const urls = [];
        const files = req.files;
        for (const file of files) {
            const { path } = file;
            const newPath = await uploader(path)
            urls.push(newPath)
            fs.unlinkSync(path)
        }
        for(const url of urls){
            if(url){                
                req.body.image = url.url;
            } 
        } 
    }
    var product = await Product.create(req.body); 
    if(product){
        res.json({
            status: "success"
        });
    }else{
        res.json({
            status: "faild"
        })
    }
};

module.exports.updateGetProduct = async (req,res)=>{
    var id = req.params.id;
    var product = await Product.findOne({where: {id_product: id}});
    if(product){
        res.json(product);
    }else{
        res.json({result: "not found data"})
    }
}

module.exports.updateProduct = async (req,res)=>{

    if(req.files){
        const uploader = async (path) => await cloudinary.uploads(path, 'Images/products');
        const urls = [];
        const files = req.files;
        for (const file of files) {
            const { path } = file;
            const newPath = await uploader(path)
            urls.push(newPath)
            fs.unlinkSync(path)
        }
        for(const url of urls){
            if(url){                
                req.body.image = url.url;
            } 
        } 
    }
    var id = req.params.id;
    var result = await Product.update(req.body,{where: {id_product: id}}); 
    if(result[0]===1){
        res.json({
          result: "success"
        });
       }else{
        res.json({
          result: "failure"
        });
       }
};

module.exports.deleteProduct = async (req,res)=>{
    var id = req.params.id;
    var result =  await Product.destroy({where: {id_product: id}}); 
    if(result!==0){
        res.json({
          result: "success"
        });
      }else{
        res.json({
          result: "failure"
        });
      }
}