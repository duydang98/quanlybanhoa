var Area = require('../models/area.model');

module.exports.listArea =  async (req,res)=>{
    var areas = await Area.findAll();
    res.json(areas);
}
module.exports.addArea = async (req,res)=>{
    var area = await Area.create(req.body);
    if(area){
        res.status(201).json(area);
    }
    
}

module.exports.updateGetArea = async(req,res)=>{
    var id = req.params.id;
    var area = await Area.findOne({where: {id_area : id}});
    res.json(area);
}

module.exports.updateArea = async(req,res)=>{
    var id = req.params.id;
    await Area.update(req.body,{where: {id_area: id}});
    res.json({
        result: "suscess"
    });
}

module.exports.deleteArea = async(req,res)=>{
    var id = req.params.id;
    await Area.destroy({where: {id_area : id}});
    res.json({
        result: "suscess"
    });
}