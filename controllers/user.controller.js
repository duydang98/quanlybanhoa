var User = require('../models/user.model');
var bcrypt = require('bcryptjs');
var cloudinary = require('../services/cloudinary');
var fs = require('fs');
var jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
dotenv.config();
var stripe = require('stripe')(process.env.STRIPE_SECRET);


module.exports.registerUser = async (req,res)=>{
    
    if(req.files){
        const uploader = async (path) => await cloudinary.uploads(path, 'Images/Users/Customers');
        const urls = [];
        const files = req.files;
        for (const file of files) {
            const { path } = file;
            const newPath = await uploader(path)
            urls.push(newPath)
            fs.unlinkSync(path)
        }
        for(const url of urls){
            if(url){                
                req.body.avatar_user = url.url;
            } 
        } 
    }
    req.body.type = 'customer';
    req.body.password = bcrypt.hashSync(req.body.password,8);

    // var customer = await stripe.customers.create(
    //     {
    //       email: req.body.email,
    //       source: 'tok_mastercard',
    //       description: 'Test Customer (created for API docs)',
    //     });
    // req.body.id_customer_stripe = customer.id;

    var user = await User.create(req.body);
     
    res.json({
        status: true,
        user:user
        });
     
}

module.exports.getUser = async (req,res)=>{
    var id = req.params.id;
    var user = await User.findOne({
        attributes: {exclude : ['password','type']},
        where:{
            id_user: id
        }
    });
    res.json(user);
}

module.exports.updateUser = async (req,res)=>{

        if(req.files){
                const uploader = async (path) => await cloudinary.uploads(path, 'Images/Users/Customers');
                const urls = [];
                const files = req.files;
                for (const file of files) {
                    const { path } = file;
                    const newPath = await uploader(path)
                    urls.push(newPath)
                    fs.unlinkSync(path)
                }
                for(const url of urls){
                    if(url){                
                        req.body.avatar_user = url.url;
                    } 
                } 
        }
    var id  = req.params.id;
   
    await User.update(req.body,{
       where : {
           id_user: id
       }
   });
    res.json({
        status: "susscess"
    });

}

module.exports.changePassword = async (req,res)=>{
    newPassword = bcrypt.hashSync(req.body.newPassword,8);
    var id  = req.params.id;
    await User.update({password: newPassword},{
       where : {
           id_user: id
       }
    });
    res.json({status: "suscess"});
}

module.exports.loginUser = async (req,res)=>{

    var user = await User.findOne({
        where:{
            email: req.body.email
        }
    });
    if(!user){
        res.status(404).json({status: "Email dose not exist"});
        return;
    }
    var isAdmin = false;
    if(user.type==='admin' || user.type==='employee' ){
        isAdmin = true;
    }
    var hashPassword = bcrypt.compareSync(req.body.password,user.password);
        if(hashPassword){
             var token = jwt.sign({ id_user: user.id_user,name_user: user.name_user ,type_user: user.type },process.env.JWT_SECRET, {
                 expiresIn: '1d' // expires in 5m
               });

               res.status(200).send({ 
                   auth: true,
                   id_user: user.id_user,
                   name_user: user.name_user,
                   email: user.email,
                   isAdmin: isAdmin,
                   token: token,});
               return;

        }else{
          res.status(400).send({ auth: false,status: "Password dose not exist",token: null });
        }
} 

//show token
module.exports.me = (req,res)=>{
    var token = req.headers['x-access-token'];
    if(!token){
      res.status(401).json({auth: false, message: 'No token'});
      return;
    };
  
    jwt.verify(token,process.env.JWT_SECRET, async (err,decoded)=>{
      if(err) return res.status(500).json({
        auth: false,
        message: "failed to authenticate token"
      });
      res.json(decoded);
     });
  }

 