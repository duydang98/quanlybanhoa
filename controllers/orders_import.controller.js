var jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
dotenv.config();
var Distributor = require('../models/distributor.model');
var Order = require('../models/order_import.model');
var helper = require('../helpers/getDate.helper');

module.exports.listOrderImport = async (req,res)=>{
    var orders = await Order.findAll()
    res.json(orders);
}

module.exports.addOrderImport = async (req,res)=>{
    var order = req.body;
    var distributor = await Distributor.findOne({where : {id_distributor : order.id_distributor}});
    order.name_distributor = distributor.name_distributor;
    order.address_distributor = distributor.address_distributor;
    order.phone_distributor = distributor.phone_distributor;
    order.date_order_import = helper();
    var token = req.headers['x-access-token'];
    jwt.verify(token,process.env.JWT_SECRET,(err,decoded)=>{
        if(err) return res.status(500).json({
          auth: false,
          message: "failed to authenticate token"
        });
        order.id_employee = decoded.id_user;
        order.name_employee = decoded.name_user;
       });
        await Order.create(order);
    res.json(order);
}

module.exports.updateGetOrderImport =async (req,res)=>{
    
    var id = req.params.id;
    var order = await Order.findOne({where: {id_order_import: id}});
    res.json(order);
}

module.exports.updateOrderImport =async (req,res)=>{
    
    var id = req.params.id;
    var order = req.body;
    if(order.id_distributor){
        var distributor = await Distributor.findOne({where : {id_distributor : order.id_distributor}});
        order.name_distributor = distributor.name_distributor;
        order.address_distributor = distributor.address_distributor;
        order.phone_distributor = distributor.phone_distributor;
    }
    

    var token = req.headers['x-access-token'];
    jwt.verify(token,process.env.JWT_SECRET,(err,decoded)=>{
        if(err) return res.status(500).json({
          auth: false,
          message: "failed to authenticate token"
        });
        order.id_employee = decoded.id_user;
        order.name_employee = decoded.name_user;
       });
    await Order.update(order,{where: {id_order_import: id}});
    //var order = await Order.findOne({where: {id_order_import: id}});
    res.json(order);
}