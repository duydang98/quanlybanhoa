var Order = require('../models/order.model');
var OrderItem = require('../models/order_item.model');
var sequelize = require('../configs/conection');
const { Op } = require("sequelize");

module.exports.statisticsRevenueYear = async (req,res)=>{
    var year = req.params.year;
    var orders = await Order.findAll({
        where: {
            [Op.and]: [
                sequelize.where(sequelize.fn('YEAR', sequelize.col('date_create_order')),year),
                { status_order: "Completed" }
              ]
        }
    });
    total_amount = 0;
    orderlist = [];
    productlist= [];
    for(const order of orders){
        total_amount += parseInt(order.total_amount);
        orderlist.push(order.id_order);
    }

    for(const id of orderlist){
        var orderitems = await OrderItem.findAll({where: {id_order: id}});
        if(orderitems[0]){
            for(const orderitem of orderitems){

                var check = 1;

                for(const product of productlist){
                    if(product.id_product === orderitem.id_product){
                        product.Quantity= product.Quantity + orderitem.quantity_product;
                        check=0;
                        break
                    }
                }
                if(check===1){
                    productlist.push({
                        id_product: orderitem.id_product,
                        Quantity: orderitem.quantity_product
                    });
                }
               
            }
            
        }
    }
    
    res.json({

        RevenueYear: year,
        Productlist: productlist,
        statisticsrevenue: total_amount
    });
}

module.exports.statisticsRevenueMonth = async (req,res)=>{
    var year = req.params.year;
    var month = req.params.month;
    var orders = await Order.findAll({
        where: {
            [Op.and]: [
                sequelize.where(sequelize.fn('YEAR', sequelize.col('date_create_order')),year),
                sequelize.where(sequelize.fn('MONTH', sequelize.col('date_create_order')),month),
                { status_order: "Completed" }
              ]
        }
    });
    total_amount = 0;
    orderlist = [];
    productlist= [];
    for(const order of orders){
        total_amount += parseInt(order.total_amount);
        orderlist.push(order.id_order);
    }

    for(const id of orderlist){
        var orderitems = await OrderItem.findAll({where: {id_order: id}});
        if(orderitems[0]){
            for(const orderitem of orderitems){

                var check = 1;

                for(const product of productlist){
                    if(product.id_product === orderitem.id_product){
                        product.Quantity= product.Quantity + orderitem.quantity_product;
                        check=0;
                        break
                    }
                }
                if(check===1){
                    productlist.push({
                        id_product: orderitem.id_product,
                        Quantity: orderitem.quantity_product
                    });
                }
               
            }
            
        }
    }
    res.json({
        RevenueMonth: month,
        RevenueYear: year,
        Productlist: productlist,
        statisticsrevenue: total_amount
    });
}