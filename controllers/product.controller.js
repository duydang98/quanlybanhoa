var Product = require('../models/product.model');

module.exports.listProduct = async (req,res)=>{
    var products = await Product.findAll({
        order:[
            ["id_product","DESC"]
        ]
    });
    res.json(products);
};

module.exports.detailProduct = async (req,res)=>{
    var id = req.params.id;
    var product = await Product.findOne({where: {id_product: id}});
    if(product){
        res.json(product);
    }else{
        res.status(404).json({result: "Product not found"})
    }
}

module.exports.getNewProduct = async (req,res)=>{
    var products = await Product.findAll({
        order:[
            ["id_product","DESC"]
        ],
        limit : 4,
    });
    res.json(products);
}