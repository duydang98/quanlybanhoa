var Comment = require('../models/comment.model');
var User = require('../models/user.model');
var jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
dotenv.config();
const { Op } = require("sequelize");

Comment.hasMany(Comment,{as: 'reply', foreignKey: 'id_parent'});
Comment.belongsTo(User,{as: 'user', foreignKey: 'id_user' });

module.exports.getComment = async (req,res)=>{

    var id = req.params.id;

    var comments = await Comment.findAll({
      
      //attributes: ['content_comment','id_user','id_product'],
      include: [
         {model: Comment, as: 'reply' },
         {model: User, as: 'user',attributes: ['name_user','avatar_user']}
      ],
      where: {id_parent: 0,id_product: id},
      
      order:[
        ['id_comment','DESC']
      ]
    });
    res.json(comments);
    
}

module.exports.getCommentReply = async (req,res)=>{

  var id = req.params.id;

    var comments = await Comment.findAll({
      include: {model: User, as: 'user',attributes: ['name_user','avatar_user']},
      where: {id_parent: id}
    });
    res.json(comments);
  
}

module.exports.addComment = async (req,res)=>{
    var token = req.headers['x-access-token'];
    jwt.verify(token,process.env.JWT_SECRET, async (err,decoded)=>{
        if(err) return res.status(500).json({
          auth: false,
          message: "failed to authenticate token"
        });
        req.body.id_user = decoded.id_user;
       });
       req.body.children_comment = 0;
       req.body.id_parent = 0;
       var comment = await Comment.create(req.body);
       res.json(comment);
}

module.exports.addReplyComment = async (req,res)=>{
    var token = req.headers['x-access-token'];
    var id = parseInt(req.params.id);
    jwt.verify(token,process.env.JWT_SECRET, async (err,decoded)=>{
        if(err) return res.status(500).json({
          auth: false,
          message: "failed to authenticate token"
        });
        req.body.id_user = decoded.id_user;
       });
       req.body.children_comment = 1;
       req.body.id_parent = id;
       var cmt = await Comment.findOne({where:{
          id_comment: id
       }});
       req.body.id_product = cmt.id_product;
       var comment = await Comment.create(req.body);
       res.json(comment);
}

module.exports.updateComment = async (req,res)=>{
    var id = req.params.id;
    //console.log(id);
    var comment = await Comment.findOne({where: {id_comment: id}});

    var token = req.headers['x-access-token'];
    jwt.verify(token,process.env.JWT_SECRET, async (err,decoded)=>{
      if(err) return res.status(500).json({
        auth: false,
        message: "failed to authenticate token"
      });
      if(decoded.id_user !== comment.id_user){
        res.json({
          result: "No permission"
        });
        return;
      }
     });
     
   //req.body.id_product =comment.id_product;
   var comment =  await Comment.update(req.body,{where: {id_comment: id}});
   if(comment[0]===1){
    res.json({
      result: "success"
    });
   }else{
    res.json({
      result: "failure"
    });
   }
   
}

module.exports.deleteComment = async (req,res)=>{
  var id = req.params.id;
  //console.log(id);
  var comment = await Comment.findOne({where: {id_comment: id}});
  
  var token = req.headers['x-access-token'];
  jwt.verify(token,process.env.JWT_SECRET, async (err,decoded)=>{
    if(err) return res.status(500).json({
      auth: false,
      message: "failed to authenticate token"
    });
    if((decoded.id_user !== comment.id_user) && decoded.type_user ==='customer'){
      res.json({
        result: "No permission"
      });
      return;
    }
   });

  var result = await Comment.destroy({where: {
    [Op.or]: [
      { id_comment: id },
      { id_parent: id }
    ]
  
  }});
  //await Comment.destroy({where: {id_parent: id}});
  //console.log(result);
  if(result!==0){
    res.json({
      result: "success"
    });
  }else{
    res.json({
      result: "failure"
    });
  }
  
}