var Distributor = require('../models/distributor.model');

module.exports.listDistributor = async(req,res)=>{
    var distributors = await Distributor.findAll();
    res.json(distributors);
}

module.exports.addDistributor = async (req,res)=>{
    var distributor = await Distributor.create(req.body);
    if(distributor){
        res.status(201).json({
            status: "susscess"
        });
    }else{
        res.json({
            status: "Faild"
        });
    }
    
}

module.exports.updateGetDistributor = async (req,res)=>{
    var id = req.params.id;
    var distributor = await Distributor.findOne({where: {id_distributor: id}});
    res.json({
        status: "susscess",
        distributor: distributor
    });
}

module.exports.updateDistributor = async (req,res)=>{
    var id = req.params.id;
    await Distributor.update(req.body,{where: {id_distributor: id}});
    res.json({
        status: "susscess"
    });
}

module.exports.deleteDistributor = async (req,res)=>{
    var id = req.params.id;
    await Distributor.destroy({where: {id_distributor: id}});
    res.json({
        status: "susscess"
    });
}