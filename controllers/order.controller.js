var Order = require('../models/order.model');
var OrderItem = require('../models/order_item.model');
var Product = require('../models/product.model');
var Promotion = require('../models/promotion.model');
var Stock = require('../models/stock.model');
var User = require('../models/user.model');
var helper = require('../helpers/getDate.helper');
var helperIdUser = require('../helpers/getIdUser.helper');
var tranpost_fee = require('../services/tranpost_fee'); 
var jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
dotenv.config();
var stripe = require('stripe')(process.env.STRIPE_SECRET);

var accountSid = process.env.ACCOUNTSID;
var authToken = process.env.AUTHTOKEN;
var twilio = require('twilio');
var client = new twilio(accountSid, authToken);

Order.hasMany(OrderItem,{as: 'order_items', foreignKey: 'id_order'});


module.exports.getOrder = async (req,res)=>{
   
    //var today =  helper();
    //console.log(today);
    var id = req.params.id;
    var order = await Order.findAll({attributes: ['id_order','status_order','date_create_order','name_recipient',
    'phone_recipient','address_recipient','total_amount','tranpost_fee','payment_method','payment_status','date_payment'],
    include: [{
      model: OrderItem , as: 'order_items'
    }]
    ,where: {id_user: id},
    order: [
      ['id_order', 'DESC'],
  ]
  });
    res.json(order);
    
}

module.exports.getOneOrder = async (req,res)=>{
  var id = req.params.id;
  var order = await Order.findOne({attributes: ['id_order','status_order','date_create_order','name_recipient',
  'phone_recipient','address_recipient','total_amount','tranpost_fee','payment_method','payment_status','date_payment'],
  include: [{
    model: OrderItem , as: 'order_items'
  }]
  ,where: {id_order: id}});
  res.json(order);
    
}

module.exports.addOrder = async (req,res)=>{

    var today =  helper();
    status_order = "Pending";
    req.body.date_create_order = today;
    req.body.id_user = helperIdUser(req.headers['x-access-token']);
    var total_amount  = 0;
    var order_items = req.body.order_items;
    
  //check order_items
    for(const item of  order_items ){
        var product = await Product.findOne({where: {id_product: item.id_product}});
        item.name_product = product.name_product;
        item.price_product = product.price_product;
        const stock = await Stock.findOne({
          where:{
            id_product: item.id_product,
            id_area: item.id_area
          }
        });
        if(!stock){
          res.status(204).send('Stock is not found');
            return;
        }
        var total_product=stock.quantity_stock;
        var quantity = total_product - item.quantity_product;
        if(total_product===0){
          res.send('Sorry, Out of stock!');
            return;
        } 

        if(item.id_promotion){
          var promotion = await Promotion.findOne({where: {id_promotion: item.id_promotion,id_product: item.id_product}});
          if(!promotion){
            res.status(204).send('Promotions is not found');
            return;
          }
          if(promotion.date_start<= today && promotion.date_finish >= today && promotion.quantity_promotion > 0){
            item.price_product = promotion.price_promotion;
            total_product = promotion.quantity_promotion;
            quantity =  total_product - item.quantity_product;
          }else{
            item.id_promotion = 0;
          }

        }
        if(quantity<0){
                res.send("Sorry, the maximum number is "+ total_product +". Hope you understand!");
                return;
            }
        item.quantity_remaining = quantity;
        item.amount = item.price_product *item.quantity_product;
        total_amount = total_amount +  item.amount;  
      
    }
    req.body.total_amount = total_amount;
    req.body.tranpost_fee = tranpost_fee(total_amount);  
    
    //payment
    if(req.body.payment_method === "COD"){
      req.body.payment_status = "Awaiting";
    }
    if(req.body.payment_method === "stripe"){

      //var user = await User.findOne({where: {id_user: req.body.id_user}});
      req.body.payment_status = "Completed";
      req.body.date_payment =today;
      
       var customer = await stripe.customers.create(
        {
          email: req.body.source_email,
          source: req.body.source_id,
          description: 'Test Customer (created for API docs)',
        });

      var charge =  await stripe.charges.create(
                {
                  amount: total_amount *100 ,
                  currency: 'usd',
                  description: 'Test Charge (created for API docs)',
                  customer: customer.id
                });

      req.body.id_charge_stripe = charge.id;
    }
    //add order
    var order = await Order.create(req.body);
    //add order_items and update quantity products
    for(const item of order_items){
        item.id_order = order.id_order;
        if(item.id_promotion && item.id_promotion !== 0){
            await Promotion.update({quantity_promotion:item.quantity_remaining},{where:{id_promotion: item.id_promotion}});         
        }
        await Stock.update({quantity_stock: item.quantity_remaining},{
          where:{
            id_product: item.id_product,
            id_area: item.id_area
          }
        });
        await OrderItem.create(item);
      }

      //send message create order
      try {
        await client.messages.create({
          body: 'The order has been successfully confirmed'+ 'Total amount is ' + req.body.total_amount,
            to: req.body.phone_recipient,  // Text this number
            from: '+12513130772' // From a valid Twilio number
        });
      } catch (error) {
        console.log(error);
      }
    
    
    req.body.id_order = order.id_order;
     res.status(201).json(req.body);
      
}

module.exports.updateInformationRecipient = async (req,res)=>{
    
    var id = req.params.id;
    var result = await Order.update(req.body,{where: {id_order: id}});
    if(result[0]===1){
        res.json({
          result: "success",
          content: req.body
        });
       }else{
        res.json({
          result: "failure"
        });
       }
    
}

//admin

module.exports.getFullOrder = async (req,res)=>{

    var orders = await Order.findAll({
      include: [{
      model: OrderItem , as: 'order_items'
    } ], order: [
      ['id_order', 'DESC'],
  ] });

    res.json(orders);

}

module.exports.updateInformationAdmin = async (req,res)=>{

    var id = req.params.id;
    if(req.body.status_order === "Completed"){
        var order = await Order.findOne({where: {id_order: id}});
       
        if(order.payment_method === "COD"){
            req.body.date_payment = req.body.date_delivery;
            req.body.payment_status= "Completed";
        }
    }

    var result = await Order.update(req.body,{where: {id_order: id}});
    if(result[0]===1){
        res.json({
          result: "success",
          content: req.body
        });
       }else{
        res.json({
          result: "failure"
        });
       }
}

module.exports.refundMoney = async (req,res)=>{
    var id = req.params.id;
    var order = await Order.findOne({where: {id_order: id}});
    req.body.payment_status = 'refund';
    req.body.date_payment = "0000-00-00";
    req.body.status_order = "Cancelled"
    if(order.payment_method === "stripe"){
        stripe.refunds.create(
            {charge: order.id_charge_stripe},
            function(err, refund) {
                if(refund){
                    stripe.refunds.retrieve(refund.id,function(err, refund) {
                       //
                      }
                    );
                }
            }
          );
    }
    var result = await Order.update(req.body,{where: {id_order: id}});
    if(result[0]===1){
        res.json({
          result: "success",
          content: req.body
        });
       }else{
        res.json({
          result: "failure"
        });
       }
}