
var Promotion = require('../models/promotion.model');
var Product = require('../models/product.model');
const { Op } = require("sequelize");
var helper = require('../helpers/getDate.helper');
module.exports.getPromotion = async (req,res)=>{
  var today = helper();
  var date = new Date(today);
  var promotion = await Promotion.findAll({
    where: {
      date_start: {[Op.lte]: date},
      date_finish: {[Op.gte]: date}}
  });
  //c2
  // var promotion = await Promotion.findAll();
  // var promotion = promotion.filter(function (x) {
  //   return x.date_start <= today && x.date_finish >= today;
  // });
  res.json(promotion);

}

module.exports.listPromotion = async (req,res)=>{
    var promotion = await Promotion.findAll();
    res.json(promotion);
}

module.exports.addPromotion = async (req,res)=>{
    product = await Product.findOne({where: {id_product: req.body.id_product}});
    if(!product && req.body.type_promotion !== "Free shipping"){
      res.json({
        result: "Product not found"
      });
      return;
    }
    if(req.body.type_promotion === "Value discount"){
      
      var price = product.price_product;
      req.body.price_promotion = price - req.body.price_promotion;
    }
    if(req.body.type_promotion === "Percentage discount"){
      var price = product.price_product;
      req.body.price_promotion = price - (req.body.price_promotion/100)*price;
    }
    var promotion = await Promotion.create(req.body);
    res.json(promotion);
}

module.exports.updatePromotion = async (req,res)=>{
    var id = req.params.id;

    var promotion =  await Promotion.update(req.body,{where: {id_promotion: id}});
   if(promotion[0]===1){
    res.json({
      result: "success"
    });
   }else{
    res.json({
      result: "failure"
    });
   }
}

module.exports.deletePromotion = async (req,res)=>{
    var id = req.params.id;
    var result = await Promotion.destroy({where: {id_promotion: id}});
    if(result!==0){
        res.json({
          result: "success"
        });
      }else{
        res.json({
          result: "failure"
        });
      }
}