var Stock = require('../models/stock.model');
var Product = require ('../models/product.model');
var Area = require ('../models/area.model');
Stock.belongsTo(Product,{as: "product", foreignKey: 'id_product'});
Stock.belongsTo(Area,{as: "area", foreignKey: 'id_area'});

module.exports.listStock = async(req,res)=>{
    var stock = await Stock.findAll({
        include: [
            {model: Product , as: "product",attributes: ['name_product']},
            {model: Area , as: "area",attributes: ['name_area']}
        ]
    });
    res.json(stock);
}

module.exports.addStock = async (req,res)=>{
    var check = await Stock.findOne({
        where: {id_product : req.body.id_product, id_area: req.body.id_area}
    });
    
    if(check){
        
        res.status(400).send("Stock already exists");
        return;
    }
    var stock = await Stock.create(req.body);
    res.status(201).json(stock);
}

module.exports.updateGetStock = async(req,res)=>{
    var id_product = req.params.id_product;
    var id_area = req.params.id_area;
    var stock = await Stock.findOne({where: {id_product : id_product, id_area: id_area}});
   
    res.json(stock);
}

module.exports.updateStock = async(req,res)=>{
    var id_product = req.params.id_product;
    var id_area = req.params.id_area;

    await Stock.update(req.body,{where: {id_product : id_product, id_area: id_area}});
    res.json({
        result: "suscess"
    });
}

module.exports.deleteStock = async(req,res)=>{
    var id_product = req.params.id_product;
    var id_area = req.params.id_area;
    await Stock.destroy({where: {id_product : id_product, id_area: id_area}});
    res.json({
        result: "suscess"
    });
}