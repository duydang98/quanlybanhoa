var ProductCategory = require('../models/product_category.model');

module.exports.listProductCategory = async(req,res)=>{
    var productcategorys = await ProductCategory.findAll();
    res.json(productcategorys);
}

module.exports.addProductCategory = async (req,res)=>{
    var productcategory = await ProductCategory.create(req.body);
    res.json(productcategory);
}

module.exports.deleteProductCategory = async(req,res)=>{
    var id_product = req.params.id_product;
    var id_category = req.params.id_category;
    await ProductCategory.destroy({where: {id_product : id_product, id_category: id_category}});
    res.json({
        result: "suscess"
    });
}