var User = require('../models/user.model');
var bcrypt = require('bcryptjs');
var cloudinary = require('../services/cloudinary');
var fs = require('fs');
var jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
dotenv.config();

   //\\//\\//Admin//\\//\\//\\
   module.exports.addEmployee =async (req,res)=>{
      
    if(req.files){
        const uploader = async (path) => await cloudinary.uploads(path, 'Images/Users/Employee');
        const urls = [];
        const files = req.files;
        for (const file of files) {
        const { path } = file;
        const newPath = await uploader(path)
        urls.push(newPath)
        fs.unlinkSync(path)
        }
        for(const url of urls){
            if(url){  
            req.body.avatar_user = url.url;
            }     
        } 
    } 
      req.body.password = bcrypt.hashSync(req.body.password,8);
      req.body.type = 'employee';

      await User.create(req.body); 
        res.json({
            status: "susscess"
        });
  }

  module.exports.listEmployee = async (req,res)=>{
      var users = await User.findAll({attributes: {exclude : ['password']},where: {type: "employee"}});
      res.json(users);
  };

  module.exports.listCustomer = async (req,res)=>{
    var customers = await User.findAll({attributes: {exclude : ['password']},where: {type: "customer"}});
    res.json(customers);
  };

  

  module.exports.detailEmployee = async (req,res)=>{
    var id = parseInt(req.params.id);
    var user = await User.findOne({ attributes: {exclude : ['password']},where: {id_user: id , type: "employee"}});
    res.json(user);
    };
  

  module.exports.deleteEmployee = async (req,res)=>{

    var id = parseInt(req.params.id);
    var result =  await User.destroy({where: {id_user: id , type: "employee"}});
    if (result) {
        res.json({
            status: "susscess"
        });
    }else{
        res.json({
            status: "not delete"
        });
    }
    
  };

  module.exports.deleteCustomer = async (req,res)=>{

    var id = parseInt(req.params.id);
    var result =  await User.destroy({where: {id_user: id , type: "customer"}});
    if (result) {
        res.json({
            status: "susscess"
        });
    }else{
        res.json({
            status: "not delete"
        });
    }
    
  };
  module.exports.updateEmployee =async (req,res)=>{

    if(req.files){
        const uploader = async (path) => await cloudinary.uploads(path, 'Images/Users/Employee');
        const urls = [];

        const files = req.files;
        for (const file of files) {
            const { path } = file;
            const newPath = await uploader(path)
            urls.push(newPath)
            fs.unlinkSync(path)
        }
        for(const url of urls){
            if(url){                
                req.body.avatar_user = url.url;
            } 
        } 
    }
        var id  = req.params.id;

        var result = await User.update(req.body,{
        where : {
            id_user: id,
            type: "employee"
        }
        });
        
        if (parseInt(result[0]) !== 0) {
            res.json({
                status: "susscess"
            });
        }else{
            res.json({
                status: "not update"
            });
        }
  }