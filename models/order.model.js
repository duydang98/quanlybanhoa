var Sequelize = require('sequelize');
var sequelize = require('../configs/conection');

module.exports = sequelize.define('orders', {
    // attributes
    id_order: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    status_order: {
        type: Sequelize.ENUM('Pending','Awaiting Shipment','Shipped','Completed','Cancelled')
    },
    date_create_order: {
        type: Sequelize.DATE
    },
    date_payment: {
        type: Sequelize.DATE
    },	
    date_delivery: {
        type: Sequelize.DATE
    },	
    name_recipient: {
        type: Sequelize.STRING
    },
    phone_recipient: {
        type: Sequelize.STRING
    }, 
    address_recipient: {
        type: Sequelize.STRING
    },	
    total_amount: {
        type: Sequelize.DECIMAL(18)
    },	
    payment_method: {
        type: Sequelize.ENUM('COD','stripe')
    },	
    payment_status:	{
        type: Sequelize.ENUM('Awaiting', 'Completed', 'refund')
    }, 
    name_shipper: {
        type: Sequelize.STRING
    },	
    phone_shipper: {
        type: Sequelize.STRING
    },
    address_shipper: {
        type: Sequelize.STRING
    },
    tranpost_fee: {
        type: Sequelize.DECIMAL(13)
    },	
    id_charge_stripe: {
        type: Sequelize.STRING
    },	
    id_user: {
        type: Sequelize.INTEGER(11),
        allowNull: false
    }

  });