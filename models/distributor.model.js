var Sequelize = require('sequelize');
var sequelize = require('../configs/conection');
module.exports = sequelize.define('distributors', {
    // attributes
    id_distributor: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    name_distributor: {
      type: Sequelize.STRING
     
    },
    phone_distributor: {
      type: Sequelize.STRING
     
    },
    address_distributor: {
        type: Sequelize.STRING
    },
    email: {
      type: Sequelize.STRING
     
    }
  });