var Sequelize = require('sequelize');
var sequelize = require('../configs/conection');
module.exports = sequelize.define('areas', {
    // attributes
    id_area: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    name_area: {
      type: Sequelize.STRING
     
    }
  });