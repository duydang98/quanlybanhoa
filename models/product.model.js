var Sequelize = require('sequelize');
var sequelize = require('../configs/conection');

module.exports = sequelize.define('products',{
    id_product: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
    name_product: {
        type: Sequelize.STRING
       
      },
    price_product: {
        type: Sequelize.DECIMAL(18,0)
    },
    unit_product: {
        type: Sequelize.STRING
    },
    description_product: {
        type: Sequelize.STRING
    },
    image: {
        type: Sequelize.STRING
    }
});