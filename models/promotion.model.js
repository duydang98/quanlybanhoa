var Sequelize = require('sequelize');
var sequelize = require('../configs/conection');
module.exports = sequelize.define('promotions', {
    // attributes
    id_promotion: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    name_promotion: {
      type: Sequelize.STRING
     
    },
    content_promotion: {
        type: Sequelize.STRING
    },
    date_start: {
        type: Sequelize.DATE
    },
    date_finish: {
        type: Sequelize.DATE
    },
    quantity_promotion:{
        type: Sequelize.INTEGER
    },
    type_promotion: {
      type: Sequelize.ENUM('Value discount','Percentage discount','BOGO','Free gift','Free shipping')
    },
    price_promotion:{
        type: Sequelize.DECIMAL(18,0)
    },
    id_product: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
  });