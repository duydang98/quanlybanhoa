var Sequelize = require('sequelize');
var sequelize = require('../configs/conection');
module.exports = sequelize.define('categorys', {
    // attributes
    id_category: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    name_category: {
      type: Sequelize.STRING
     
    },
    type_category: {
      type: Sequelize.ENUM('color','flowercategory','theme')
    }
  });