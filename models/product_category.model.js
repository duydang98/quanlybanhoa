var Sequelize = require('sequelize');
var sequelize = require('../configs/conection');
module.exports = sequelize.define('products_categorys', {
    // attributes
    id_product: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    id_category: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        primaryKey: true
    }
  });