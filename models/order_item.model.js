var Sequelize = require('sequelize');
var sequelize = require('../configs/conection');

module.exports = sequelize.define('orders_item', {
    // attributes
    id_order: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    id_product: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        primaryKey: true
    },
    name_product: {
        type: Sequelize.STRING
    },
    quantity_product: {
        type: Sequelize.INTEGER(11)
    },
    price_product: {
        type: Sequelize.DECIMAL(18,0)
    },
    id_area: {
        type: Sequelize.INTEGER(11)
    },
    id_promotion: {
        type: Sequelize.INTEGER(11)
    },
    amount: {
        type: Sequelize.DECIMAL(18,0)
    }
  });