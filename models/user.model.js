var Sequelize = require('sequelize');
var sequelize = require('../configs/conection');
module.exports = sequelize.define('Users', {
    // attributes
    id_user: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    name_user: {
      type: Sequelize.STRING
     
    },
    birthday_user: {
        type: Sequelize.DATE
    },
    sex_user: {
      type: Sequelize.STRING
     
    },
    phone_user: {
      type: Sequelize.STRING
     
    },
    address_user: {
        type: Sequelize.STRING
    },
    email: {
      type: Sequelize.STRING
     
    },
    password: {
      type: Sequelize.STRING
     
    },
    avatar_user: {
      type: Sequelize.STRING
     
    },
    type: {
        type: Sequelize.ENUM('customer','employee','admin')
    },
    id_customer_stripe: {
      type: Sequelize.STRING
     
    }
  });