var Sequelize = require('sequelize');
var sequelize = require('../configs/conection');

module.exports = sequelize.define('orders_imports_item', {
    // attributes
    id_order_import: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    id_product: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        primaryKey: true
    },
    name_product: {
        type: Sequelize.STRING
    },
    quantity_import: {
        type: Sequelize.INTEGER(11)
    },
    unit_price: {
        type: Sequelize.DECIMAL(18,0)
    },
    expiry_date: {
        type: Sequelize.DATE
    }

  });