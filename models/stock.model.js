var Sequelize = require('sequelize');
var sequelize = require('../configs/conection');
module.exports = sequelize.define('stocks', {
    // attributes
    id_product: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    id_area: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        primaryKey: true
    },
   quantity_stock : {
        type: Sequelize.INTEGER(11),
        allowNull: false
    },
    expiry_date_stock: {
        type: Sequelize.DATE
    }
  });