var Sequelize = require('sequelize');
var sequelize = require('../configs/conection');

module.exports = sequelize.define('orders_import', {
    // attributes
    id_order_import: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    name_order_import: {
        type: Sequelize.STRING
    },
    date_order_import: {
        type: Sequelize.DATE
    },
    id_distributor: {
        type: Sequelize.INTEGER(11)
    },
    name_distributor: {
      type: Sequelize.STRING
     
    },
    phone_distributor: {
      type: Sequelize.STRING
     
    },
    address_distributor: {
        type: Sequelize.STRING
    },
    id_employee: {
        type: Sequelize.INTEGER(11)
    },
    name_employee: {
        type: Sequelize.STRING
    }
  });