var Sequelize = require('sequelize');
var sequelize = require('../configs/conection');
module.exports = sequelize.define('comments', {
    // attributes
    id_comment: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    content_comment: {
      type: Sequelize.STRING
     
    },
    children_comment: {
      type: Sequelize.BOOLEAN
    },
    id_parent: {
        type: Sequelize.INTEGER
    },
    id_user: {
        type: Sequelize.INTEGER
    },
    id_product: {
        type: Sequelize.INTEGER
    }
  });