var validator = require('../validate/distributor.validate');
var PhoneNumber = require('awesome-phonenumber');
module.exports.authDistributor = (req,res,next)=>{
    var distributor = req.body;
    var result = validator.distributorSchema.validate(distributor);
    var pn = new PhoneNumber(distributor.phone_distributor,'VN');
    distributor.phone_distributor = pn.getNumber();
    if(result.error || pn.isValid()===false){
        res.status(400).json({result: "Invalid data"});
        return;
    }
    
    next();
};