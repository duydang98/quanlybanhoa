var validator = require('../validate/product.validate');


module.exports.authProduct = (req,res,next)=>{
    var product = req.body;
    var value = validator.productSchema.validate(product);
    if (value.error) {
        res.status(400).json({result: "Invalid data", error: value.error});
        return;
    }
    next();
}

