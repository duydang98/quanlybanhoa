var Joi = require('@hapi/joi');
var validator = Joi.object().keys({
    content_comment: Joi.string().required()
})
module.exports.checkComment = (req,res,next)=>{
    var comment = req.body;
    var result = validator.validate(comment);
    if(result.error){
        res.json({result: "Invalid data"});
        return;
    }
    next();
}