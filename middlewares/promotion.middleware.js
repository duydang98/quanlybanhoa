var validator = require('../validate/promotion.validate');

module.exports.checkPromotion = (req,res,next)=>{
    var promotion = req.body;
    var value = validator. promotionSchema.validate( promotion);
    if (value.error) {
        res.json({result: "Invalid data", error: value.error});
        return;
    }
    next();
}