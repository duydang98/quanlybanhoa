var validator = require('../validate/order.validate');
var jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
var Order = require('../models/order.model');
var User = require('../models/user.model');
var PhoneNumber = require('awesome-phonenumber');
dotenv.config();

module.exports.checkTokenAdd = (req,res,next)=>{

    var token = req.headers['x-access-token'];
    if(!token){
      res.status(401).json({auth: false, message: 'No token'});
      return;
    };
  
    jwt.verify(token,process.env.JWT_SECRET, async (err,decoded)=>{
      if(err) return res.status(500).json({
        auth: false,
        message: "failed to authenticate token"
      });
   
    var user = await User.findOne({
        attributes: {exclude : ['password','type']},
        where : { id_user: decoded.id_user}
      });
  
      if(!user){
            res.status(400).json({status: "No user"})
          return;
      }
      next();

     });
  }


module.exports.checkTokenUpdate = async (req,res,next)=>{
    var token = req.headers['x-access-token'];
    if(!token){
        res.status(401).json({auth: false, message: 'No token'});
        return;
    };

    jwt.verify(token,process.env.JWT_SECRET, async (err,decoded)=>{
        if(err) return res.status(500).json({
        auth: false,
        message: "failed to authenticate token"
        });

  
    var id = req.params.id;   
    var order = await Order.findOne({
        where : { id_order: id}
        });

    if(!order){
          res.json({status: "No order"})
        return;
    }
    if(order.id_user !== decoded.id_user){
        res.json({status: "You do not have access"})
        return;
    }

    next();
    
  //   res.json(user);
   
   });
}

module.exports.checkAddOrder = (req,res,next)=>{
    var order = req.body;
    var value = validator.orderUser.validate(order);
    var pn = new PhoneNumber(order.phone_recipient,'VN');
    order.phone_recipient = pn.getNumber();
    if(value.error || pn.isValid()===false){
        res.json({result: "Invalid data"});
        return;
    }
    next();
}

module.exports.checkUpdateOrder = (req,res,next)=>{
    var order = req.body;
    var value = validator.updateOrderUser.validate(order);
    if(value.error){
        res.json({result: "Invalid data"});
        return;
    }
    next();
}

module.exports.checkUpdateOrderAdmin = (req,res,next)=>{
    var order = req.body;
    var value = validator.updateOrderAdmin.validate(order);
    if(value.error){
        res.json({result: "Invalid data"});
        return;
    }
    next();
}