var validator = require('../validate/User.validate');
var User = require('../models/user.model');
var Order = require('../models/order.model');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
var PhoneNumber = require('awesome-phonenumber');
dotenv.config();

module.exports.authRegister = async (req,res,next)=>{
    var user = req.body;
    var value = validator.userSchema.validate(user);
    var pn = new PhoneNumber(user.phone_user,'VN');
    user.phone_user = pn.getNumber();
    if(value.error || pn.isValid()===false){
        res.status(400).json({result: "Invalid data"});
        return;
    }
    if(value.error){
        res.status(400).json({result: "Invalid data"});
        return;
    }
    var value =  await User.findOne({
        where:{
            email : user.email
        }
    });

    if(value){
        res.status(400).json({result: "Email already exists"});
        return;
    }

    next();
}

module.exports.authUpdate = (req,res,next)=>{
    var user = req.body;
    var value = validator.userUpdateSchema.validate(user);
    if(value.error){
        res.status(400).json({result: "Invalid data"});
        return;
    }
    next();
}

module.exports.authChangePassword = async (req,res,next)=>{
    var data = req.body;
    var id = req.params.id;

    var value = validator.passwordChangeSchema.validate(data);
    if(value.error){
        res.status(400).json({result: "Invalid data"});
        return;
    }

    var user = await User.findOne({where:{id_user: id}});
    var hashPassword = bcrypt.compareSync(data.password,user.password);
    if(!hashPassword){
        res.json({status: "incorrect password"});
        return;
    }
    next();

}

module.exports.checkToken = (req,res,next)=>{

    var token = req.headers['x-access-token'];
    if(!token){
      res.status(401).json({auth: false, message: 'No token'});
      return;
    };
  
    jwt.verify(token,process.env.JWT_SECRET, async (err,decoded)=>{
      if(err) return res.status(500).json({
        auth: false,
        message: "failed to authenticate token"
      });

    var id = parseInt(req.params.id);
      
    if(decoded.id_user !== id){
        res.status(403).json({result: "No permission"});
        return;
    }
     
    var user = await User.findOne({
        attributes: {exclude : ['password','type']},
        where : { id_user: decoded.id_user}
      });

      if(!user){
            res.status(404).json({status: "No user"})
          return;
      }
      next();
      
    //   res.json(user);
     
     });
}

module.exports.checkTokenAdmin = (req,res,next)=>{

    var token = req.headers['x-access-token'];
    if(!token){
      res.status(401).json({auth: false, message: 'No token'});
      return;
    };
  
    jwt.verify(token,process.env.JWT_SECRET, async (err,decoded)=>{
      if(err) return res.status(500).json({
        auth: false,
        message: "failed to authenticate token"
      });
      //res.status(200).json(decoded);
      var typeuser = decoded.type_user;

      if(typeuser !== 'admin'){
          res.status(403).json({
            result: "No permission"
          });
          return;
      }
     
      next();
      
    //   res.json(user);
     
     });
}

module.exports.checkTokenEmployee = (req,res,next)=>{

  var token = req.headers['x-access-token'];
  if(!token){
    res.status(401).json({auth: false, message: 'No token'});
    return;
  };

  jwt.verify(token,process.env.JWT_SECRET, (err,decoded)=>{
    if(err) return res.status(500).json({
      auth: false,
      message: "failed to authenticate token"
    });
    //res.status(200).json(decoded);
    var typeuser = decoded.type_user;

    if(typeuser === 'customer'){
        res.status(403).json({
          result: "No permission"
        });
        return;
    }
   
    next();
    
  //   res.json(user);
   
   });
}

module.exports.checkTokenComment = (req,res,next)=>{

  var token = req.headers['x-access-token'];
  if(!token){
    res.status(401).json({auth: false, message: 'No token'});
    return;
  };

  jwt.verify(token,process.env.JWT_SECRET, async (err,decoded)=>{
    if(err) return res.status(500).json({
      auth: false,
      message: "failed to authenticate token"
    });

  
   
  var user = await User.findOne({
      attributes: {exclude : ['password','type']},
      where : { id_user: decoded.id_user}
    });

    if(!user){
          res.status(404).json({status: "No user"})
        return;
    }
    next();
    
  //   res.json(user);
   
   });
}

module.exports.checkLogin = async (req,res,next)=>{
  var user = req.body;
  var value = validator.loginSchema.validate(user);

  if(value.error){
      res.status(400).json({result: "Invalid data"});
      return;
  }
  next();

}


module.exports.checkTokenOrder = async (req,res,next)=>{

  var token = req.headers['x-access-token'];
  if(!token){
    res.status(401).json({auth: false, message: 'No token'});
    return;
  };

  jwt.verify(token,process.env.JWT_SECRET, async (err,decoded)=>{
    if(err) return res.status(500).json({
      auth: false,
      message: "failed to authenticate token"
    });

    var id = parseInt(req.params.id);
    var order = await Order.findOne({
      where : { id_order: id}
    });
    if(!order){
      res.status(404).json({status: "No order"})
    return;
    }
    
    if(decoded.id_user !== order.id_user){
        res.status(403).json({result: "No permission"});
        return;
    }
     
   

     
  
   
  
    next();
    
  //   res.json(user);
   
   });
}